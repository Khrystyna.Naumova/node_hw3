import jwt from 'jsonwebtoken';

export function getRoles(roles) {
  return function(req, res, next) {
    if (req.method === "OPTIONS") {
       next()
    }
    console.log(roles) //
    try {
      const {authorization} = req.headers
      const token = authorization.split(' ')[1]
      if (!token) {
        return res.status(400).json({message: 'Not found'})
      }
      const decoded = jwt.verify(token, 'secret');
      const userRole = decoded.role
      console.log(userRole) //
      let hasRole = false;
      roles.forEach(userRole => {
        if (roles.includes(userRole)) {
          hasRole = true
        } 
      });
        if(!roles.includes(userRole)) {
        return res.status(400).json({message:'You do not have access'})
        }
      next();
    } catch(e) {
      console.log(e)
      res.status(500).json({message:'Server error'})
    }
  }
}

