import User from './models/User.js';
import Role from './models/Role.js';
import Truck from './models/Truck.js';
import Load from './models/Load.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { validationResult } from 'express-validator';

//AUTH
const generateAccesToken = (id, role) => {
  const payload = {
    id,
    role
  }
  return jwt.sign(payload, 'secret', { expiresIn: '24h' });
}

class authController {
  async registration(req, res) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ message: 'Registration error ', errors });
      }
      const { email, password, role } = req.body //user@gmail.com 123 USER, driver@gmail.com 124 DRIVER
      const candidate = await User.findOne({ email });
      if (candidate) {
        return res.status(400).json({ message: 'User already exists ' });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const userRole = new Role({ value: req.body.role })
      await userRole.save()
      const user = new User({ email, password: hashPassword, created_date: new Date(), role: userRole.value });
      await user.save()
      Role.collection.dropIndexes()
      return res.json({ message: 'Profile created successfully' });
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async login(req, res) {
    try {
      const { email, password } = req.body
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(400).json({ message: `The user ${email} can't be found` });
      }
      const validPassword = bcrypt.compareSync(password, user.password)
      if (!validPassword) {
        return res.status(400).json({ message: 'Incorrect password' });
      }
      const jwt_token = generateAccesToken(user._id, user.role)
      return res.json({ jwt_token: jwt_token });
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  //USER
  async getUserProfile(req, res) {
    try {
      const { authorization } = req.headers
      if (!authorization) {
        return res.status(400).json({ message: 'Token was not found' })
      }
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const user = await User.findOne({ _id: decoded.id }).exec()
      return res.json({ user: user })
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async deleteUserProfile(req, res) {
    try {
      const { authorization } = req.headers
      if (!authorization) {
        return res.status(400).json({ message: 'Not found' })
      }
      const token = authorization.split(' ')[1]
      const verified = jwt.verify(token, 'secret')
      await User.findByIdAndDelete({ _id: verified.id })
      return res.json({ message: 'Profile deleted successfully' })
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async changePassword(req, res) {
    try {
      const { authorization } = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const newPassword = req.body.newPassword
      const hashNewPassword = bcrypt.hashSync(newPassword, 7);
      const result = await User.findByIdAndUpdate({ _id: decoded.id }, { $set: { password: hashNewPassword } })
      return res.json({ message: 'Password changed successfully' })
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: 'Server error' })
    }
  }

  //TRUCKS
  async addTruck(req, res) {
    try {
      const { authorization } = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const created_by = decoded.id
      const type = req.body.type
      const truck = new Truck({ created_by, assigned_to: null, type, status: 'IS', created_date: new Date() })
      const typeArray = Truck.prototype.schema.obj.type.enum
      if (!typeArray.includes(type)) {
        return res.status(400).json({ message: 'Incorrect type' })
      }
      truck.user = created_by;
      await truck.save()
      return res.json({ message: 'Truck created successfully' });
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async getTrucks(req, res) {
    try {
      const trucks = await Truck.find({})
      return res.json({ trucks: trucks });
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async getOneTruck(req, res) {
    try {
      const { id } = req.params
      if (!id) {
        return res.status(400).json({ message: 'Not found' })
      }
      const truck = await Truck.findById(id)
      return res.json(truck)
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async updateTruck(req, res) {
    try {
      const type = req.body
      const { id } = req.params
      const typeArray = Truck.prototype.schema.obj.type.enum
      const typeText = req.body.type
      const truck = await Truck.findById(id).exec()
      if (!typeArray.includes(typeText) || truck.assigned_to !== null) {
        return res.status(400).json({ message: 'Incorrect type OR You cannot update assigned trucks' })
      }
      await truck.updateOne(type, { new: true })
      return res.json({ message: 'Truck details changed successfully' })
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: 'Server error' })
    }
  }

  async deleteTruck(req, res) {
    try {
      const { id } = req.params
      const truck = await Truck.findById(id)
      if (!truck || truck.assigned_to !== null) {
        return res.status(400).json({ message: 'You cannot delete assigned trucks' })
      }
      await truck.deleteOne({})
      return res.json({ message: 'Truck deleted successfully' })
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async assignTruck(req, res) {
    try {
      const { id } = req.params
      const { authorization } = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const driver_id = decoded.id
      let truck = await Truck.exists({ assigned_to: driver_id })
      if (truck) {
        return res.status(400).json({ message: 'You have already had assigned truck' })
      }
      return await Truck.findByIdAndUpdate(id, { assigned_to: driver_id }, { new: true }), res.json({ message: 'Truck assigned successfully' });
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: 'Server error' })
    }
  }

  //LOAD
  async addLoad(req, res) {
    try {
      const { authorization } = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const shipperId = decoded.id
      const { name, payload, pickup_address, delivery_address, dimensions: { width, length, height } } = req.body
      const load = new Load({ created_by: shipperId, assigned_to: null, status: 'NEW', state: 'null', name, payload, pickup_address, delivery_address, dimensions: { width, length, height }, logs: null, created_date: new Date() })
      load.user = shipperId;
      await load.save()
      return res.json({ message: 'Load created successfully' });
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: 'Server error' })
    }
  }

  async getLoads(req, res) { // for driver - ASSIGNED, SHIPPED, for shippers - all
    try {
      const searchStatus = req.query.status
      const loads = await Load.find({})
      if(searchStatus) {
        const loadsQ = await Load.find({ status: searchStatus }).exec()
        return res.json({ loads: loadsQ });
      }
      return res.json({ loads: loads });
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async getActiveLoads(req, res) { //load that assigned_to Driver and with OL +
    try {
      const { authorization } = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const driverId = decoded.id
      const load = await Load.find({ assigned_to: driverId, status: 'ASSIGNED' })
      return res.json({ load: load });
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async nextLoadState(req, res) {
    try {
      const { authorization } = req.headers
      const token = authorization.split(' ')[1]
      const decoded = jwt.verify(token, 'secret')
      const driverId = decoded.id
      const load = await Load.findOne({ assigned_to: driverId, status: 'ASSIGNED' })
      console.log(load)
      if (load.state === 'En route to Pick Up') {
        await load.updateOne({ state: 'Arrived to Pick Up' }, { new: true })
        return res.json({ message: 'Load state changed to Arrived to Pick Up' })
      }
      if (load.state === 'Arrived to Pick Up') {
        await load.updateOne({ state: 'En route to delivery' }, { new: true })
        return res.json({ message: 'Load state changed to En route to delivery' })
      }
      if (load.state === 'En route to delivery') {
        await load.updateOne({ state: 'Arrived to delivery', status: 'SHIPPED' }, { new: true })
        const truck = await Truck.findOne({ assigned_to: driverId, status: 'OL' })
        await truck.updateOne({ assigned_to: null, status: 'IS' }, { new: true })
        return res.json({ message: 'Arrived to delivery' })
      }
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async getOneLoad(req, res) {
    try {
      const { id } = req.params
      if (!id) {
        return res.status(400).json({ message: 'Not found' })
      }
      const load = await Load.findById(id).exec()
      return res.json(load)
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async updateOneLoad(req, res) {
    try {
      const { name, payload, pickup_address, delivery_address, dimensions: { width, length, height } } = req.body
      const { id } = req.params
      const load = await Load.findById(id).exec()
      if (!id || load.status !== 'NEW') {
        return res.status(400).json({ message: 'You can update load only with status NEW' })
      }
      await load.updateOne({ name, payload, pickup_address, delivery_address, dimensions: { width, length, height } }, { new: true })
      return res.json({ message: 'Load details changed successfully' })
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async deleteOneLoad(req, res) {
    try {
      const { id } = req.params
      const load = await Load.findById(id).exec()
      if (!load || load.status !== 'NEW') {
        return res.status(400).json({ message: 'You can delete load only with NEW status' })
      }
      await load.deleteOne({})
      return res.json({ message: 'Load deleted successfully' })
    } catch (e) {
      res.status(500).json({ message: 'Server error' })
    }
  }

  async postLoad(req, res) {
    try {
      const { id } = req.params
      const load = await Load.findByIdAndUpdate(id, { status: 'POSTED' }, { new: true })
      const truck = await Truck.findOne({ status: 'IS' }).exec()
      console.log(truck)
      if (truck.assigned_to === null) {
        await load.updateOne({ status: 'NEW', state: null }, { new: true })  //??
        return res.status(400).json({ message: 'There is no assigned truck' })
      }
      await truck.updateOne({ status: 'OL' }, { new: true })
      await load.updateOne({ assigned_to: truck.created_by, status: 'ASSIGNED', state: 'En route to Pick Up' }, { new: true })
      return res.json({ message: 'Load posted successfully', driver_found: true })
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: 'Server error' })
    }
  }

  //GET ??

}

const controller = new authController();
export default controller
