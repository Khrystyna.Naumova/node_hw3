import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const states = Object.freeze({
  state0: 'null',
  state1: 'En route to Pick Up', 
  state2: 'Arrived to Pick Up', 
  state3: 'En route to delivery',
  state4: 'Arrived to delivery'
});

const statuses = Object.freeze({
  status1: 'NEW', 
  status2: 'POSTED',
  status3: 'ASSIGNED',
  status4: 'SHIPPED'
});

const Load = new Schema({
  created_by: {type : String, ref: 'User'},
  assigned_to: {type: String, ref: 'User'},
  status: {
    type: String, 
    enum: Object.values(statuses), 
    required: true
  },
  state: {
    type: String, 
    enum: Object.values(states), 
    required: true
  },
  name: {type : String},
  payload: {type: Number},
  pickup_address: {type: String},
  delivery_address: {type: String},
  dimensions: {
    width: Number,
    length: Number, 
    height: Number
  }, 
  logs: [{message: String, time: Date}],
  created_date: {type: Date, required: true}
})

export default model('Load', Load);
