import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const roles = Object.freeze({
  // v1: 'USER', 
  v2: 'DRIVER',
  v3: 'SHIPPER'
});

const Role = new Schema({
  value: {
    type: String, 
    enum: Object.values(roles), 
    required: true}
})

export default model('Role', Role)