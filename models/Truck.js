import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const types = Object.freeze({
  type1: 'SPRINTER', 
  type2: 'SMALL STRAIGHT', 
  type3: 'LARGE STRAIGHT'
});

const statuses = Object.freeze({
  status1: 'OL', 
  status2: 'IS'
});

const Truck = new Schema({
  created_by: {type : String, ref: 'User', required: true},
  assigned_to: {type: String, ref: 'User'},
  type: {
    type: String, 
    enum: Object.values(types), 
    required: true
  },
  status: {
    type: String, 
    enum: Object.values(statuses), 
    required: true
  },
  created_date: {type: Date, required: true}
})

export default model('Truck', Truck);
