import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const User = new Schema({
  role: {type: String, ref: 'Role'},
  email: {type: String, unique: true, required: true},
  password: {type: String, required: true},
  created_date: {type: Date, required: true}
 
})

export default model('User', User)