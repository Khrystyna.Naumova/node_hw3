import { Router } from 'express'
import controller from './authController.js'
import {check} from 'express-validator'
import {getRoles} from './roleMiddleware.js'
const router = new Router()

//AUTH
router.post('/api/auth/register', [
  check('email', 'Email cannot be empty').notEmpty(),
  check('password', 'Password should be more 2 and less than 7').isLength({ min:2, max:7 })
],
controller.registration);
router.post('/api/auth/login', controller.login);

//USER
router.get('/api/users/me', controller.getUserProfile);
router.delete('/api/users/me', controller.deleteUserProfile);
router.patch('/api/users/me', controller.changePassword)

//TRUCKS
router.post('/api/trucks', getRoles(['DRIVER']), controller.addTruck);
router.get('/api/trucks', getRoles(['DRIVER']), controller.getTrucks);
router.get('/api/trucks/:id', getRoles(['DRIVER']), controller.getOneTruck);
router.put('/api/trucks/:id', getRoles(['DRIVER']), controller.updateTruck);
router.delete('/api/trucks/:id', getRoles(['DRIVER']), controller.deleteTruck);
router.post('/api/trucks/:id/assign', getRoles(['DRIVER']), controller.assignTruck);

//LOADS
router.get('/api/loads', getRoles(['DRIVER', 'SHIPPER']), controller.getLoads);
router.post('/api/loads', getRoles(['SHIPPER']), controller.addLoad);
router.get('/api/loads/active', getRoles(['DRIVER']), controller.getActiveLoads);
router.patch('/api/loads/active/state', getRoles(['DRIVER']), controller.nextLoadState)
router.get('/api/loads/:id', getRoles(['SHIPPER']), controller.getOneLoad);
router.put('/api/loads/:id', getRoles(['SHIPPER']), controller.updateOneLoad);
router.delete('/api/loads/:id', getRoles(['SHIPPER']), controller.deleteOneLoad);
router.post('/api/loads/:id/post', getRoles(['SHIPPER']), controller.postLoad);

export default router